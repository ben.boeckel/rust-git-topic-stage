include:
    # Metadata shared by many jobs
    - local: .gitlab/rules.yml
    - local: .gitlab/artifacts.yml

    # Builds.
    - local: .gitlab/git.yml
    - local: .gitlab/cargo.yml

stages:
    - prep
    - build
    - test
    - publish

.rust_minimum_mindeps:
    image: "rust:1.53"

    variables:
        CARGO_UPDATE_POLICY: newest
        GIT_CLONE_PATH: $CI_BUILDS_DIR/rust

.rust_minimum:
    image: "rust:1.60"

    variables:
        CARGO_UPDATE_POLICY: newest
        GIT_CLONE_PATH: $CI_BUILDS_DIR/rust

.rust_stable:
    image: "rust:latest"

    variables:
        CARGO_UPDATE_POLICY: newest
        GIT_CLONE_PATH: $CI_BUILDS_DIR/rust

.rust_nightly:
    extends: .rust_stable

    image: "rustlang/rust:nightly"

cache-newest:prep:
    extends:
        - .rust_stable
        - .cargo_fetch_job
        - .cargo_build_tags
        - .cargo_fetch_artifacts
        - .run_automatically

cache-mindeps:prep:
    extends:
        - .rust_nightly
        - .cargo_fetch_job
        - .cargo_build_tags
        - .cargo_fetch_artifacts
        - .run_automatically
    variables:
        GENERATE_LOCKFILE_ARGS: "-Z minimal-versions"
        CARGO_UPDATE_POLICY: mindeps

clippy:build:
    extends:
        - .rust_stable
        - .cargo_clippy_job
        - .cargo_build_tags
        - .run_automatically
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

.cargo_audit_ignore:
    variables:
        # Ignored advisories
        #
        # Regexes with large repetitions on empty sub-expressions take a very
        # long time to parse
        # https://rustsec.org/advisories/RUSTSEC-2022-0013
        # Not relevant; only static regexes are used here.
        #
        # Use-after-free due to a lifetime error in `Vec::into_iter()`
        # https://rustsec.org/advisories/RUSTSEC-2022-0078
        CARGO_AUDIT_ARGS: --ignore RUSTSEC-2022-0013 --ignore RUSTSEC-2022-0078

audit-mindeps:build:
    extends:
        - .rust_minimum_mindeps
        - .cargo_audit_job
        - .cargo_build_tags
        - .run_automatically
        - .cargo_audit_ignore
    dependencies:
        - cache-mindeps:prep
    needs:
        - cache-mindeps:prep
    variables:
        CARGO_UPDATE_POLICY: mindeps

audit-newest:build:
    extends:
        - .rust_stable
        - .cargo_audit_job
        - .cargo_build_tags
        - .run_automatically
        - .cargo_audit_ignore
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

minimum:build:
    extends:
        - .rust_minimum
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

minimum:test:
    extends:
        - .rust_minimum
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    dependencies:
        - minimum:build
    needs:
        - minimum:build

stable:build:
    extends:
        - .rust_stable
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

stable:test:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    dependencies:
        - stable:build
    needs:
        - stable:build

tarpaulin:build:
    extends:
        - .rust_stable
        - .cargo_tarpaulin_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

tarpaulin:test:
    extends:
        - .rust_stable
        - .cargo_tarpaulin_test_job
        - .cargo_privileged_tags
        - .run_automatically
    dependencies:
        - tarpaulin:build
    needs:
        - tarpaulin:build

nightly:build:
    extends:
        - .rust_nightly
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    dependencies:
        - cache-newest:prep
    needs:
        - cache-newest:prep

nightly:test:
    extends:
        - .rust_nightly
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    dependencies:
        - nightly:build
    needs:
        - nightly:build

mindeps:build:
    extends:
        - .rust_minimum_mindeps
        - .cargo_build_job
        - .cargo_build_tags
        - .cargo_build_artifacts
        - .run_automatically
    dependencies:
        - cache-mindeps:prep
    needs:
        - cache-mindeps:prep

mindeps:test:
    extends:
        - .rust_minimum_mindeps
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    dependencies:
        - mindeps:build
    needs:
        - mindeps:build

git:prep:
    extends:
        - .git_build_master
        - .cargo_build_tags
        - .run_automatically

git-master:test:
    extends:
        - .rust_stable
        - .cargo_test_job
        - .cargo_build_tags
        - .cargo_test_artifacts
        - .run_automatically
    dependencies:
        - git:prep
        - stable:build
    needs:
        - git:prep
        - stable:build

publish:
    extends:
        - .rust_stable
        - .cargo_publish_job
        - .cargo_build_tags
        - .run_automatically
    dependencies: []
    needs:
        - audit-mindeps:build
        - audit-newest:build
        - clippy:build
        - git-master:test
        - mindeps:test
        - nightly:test
        - stable:test
        - tarpaulin:test
